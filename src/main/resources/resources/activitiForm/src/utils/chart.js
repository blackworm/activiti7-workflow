// import * as echarts from 'echarts'
import { hexToRgb } from '@/utils'

/**
 * 饼状图、环形图
 * @params {*}
 * @returns
 */
export const pieChart = (params) => {
  const option = {
    color: params.colors || ['#3FB1E3', '#6BE6C1', '#626C91', '#A0A7E6', '#C4EBAD'],
    title: params.title,
    tooltip: {
      confine: true,
      trigger: 'item',
      formatter: params.tooltip || function(item) {
        return item.marker + ' ' + item.name + ': ' + item.value
      }
    },
    legend: params.customLegend ? [
      {
        orient: 'vertical',
        right: '30%',
        top: '25%',
        itemWidth: 12,
        itemHeight: 12,
        textStyle: {
          color: '#fff',
          lineHeight: 20
        },
        data: params.customLegendData
      },
      {
        orient: 'vertical',
        right: '5%',
        top: '25%',
        itemWidth: 12,
        itemHeight: 12,
        textStyle: {
          color: '#fff',
          lineHeight: 20
        },
        data: params.customLegendDatas
      }
    ] : {
      show: params.isShowLegend || false,
      type: params.legendType || 'scroll',
      orient: params.legendOrient || 'vertical', // 垂直排列
      x: 'right',
      y: params.legendY || 'center',
      itemWidth: 12,
      itemHeight: 12,
      textStyle: {
        color: '#999'
      }
    },
    series: [
      {
        name: params.seriesName,
        type: 'pie',
        roseType: params.roseType || false ,
        radius: params.radius || '80%',
        center: params.center || ['38%', '50%'],
        avoidLabelOverlap: false,
        label: {
          normal: {
            show: params.showLabel || false,
            position: params.showLabelLine ? 'outside' : 'inside',
            formatter: '{b}'
          }
        },
        labelLine: {
          normal: {
            show: params.showLabelLine || false,
            length: 2,
            length2: 5
          }
        },
        data: params.data
      }
    ]
  }
  return option
}

/**
 * 折线图、曲线图
 * @params {*}
 * @returns
 */
export const lineChart = (params) => {
  const colors = ['#3FB1E3', '#72E7C4', '#6C7598', '#A5ABE7', '#CEEEBC', '#7256ff', '#9967bd', '#ff9d4e']
  const option = {
    legend: {
      show: params.isShowLegend || false,
      type: 'scroll',
      orient: 'horizontal', // 水平排列
      x: 'center',
      y: 'top',
      itemHeight: 8,
      pageTextStyle: {
        color: '#999999'
      },
      textStyle: {
        color: '#999999'
      }
    },
    tooltip: {
      confine: true,
      trigger: 'axis'
    },
    grid: {
      containLabel: true,
      top: params.gridTop || 50,
      right: params.gridRight || 30,
      left: params.gridLeft || 30,
      bottom: params.gridBottom || 10
    },
    xAxis: {
      type: 'category',
      axisLine: {
        show: true,
        lineStyle: {
          color: '#CCCCCC'
        }
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        color: '#999999'
      },
      splitLine: {
        show: false
      },
      boundaryGap: false,
      data: params.xAxisData
    },
    yAxis: {
      name: params.yName || '',
      nameTextStyle: {
        color: '#CCCCCC'
      },
      type: 'value',
      splitLine: {
        show: true,
        lineStyle: {
          type: 'solid',
          color: '#EEEEEE',
          opacity: 1
        }
      },
      axisLine: {
        show: params.yAxisLineShow || true,
        lineStyle: {
          color: '#CCCCCC'
        }
      },
      axisLabel: {
        show: true,
        textStyle: {
          color: '#999999'
        }
      },
      axisTick: {
        show: false
      }
    },
    dataZoom: [
      {
        show: params.isShowDataZoom || false,
        type: 'slider',
        start: 0,
        end: params.zoomEnd || 100
      }
    ],
    series: params.seriesData.map((v, i) => {
      const defaultSeries = {
        name: v.name || '',
        type: 'line',
        symbol: params.isSymbol ? 'circle' : 'none',
        smooth: params.isSmooth || false,
        symbolSize: 6,
        itemStyle: {
          color: colors[i],
          borderColor: colors[i],
          borderWidth: 2
        },
        lineStyle: {
          color: colors[i]
        },
        areaStyle: {
          opacity: params.isArea ? 0.7 : 0,
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [
              { offset: 0, color: hexToRgb(colors[i], '0.9') },
              { offset: 0.5, color: hexToRgb(colors[i], '0.7') },
              { offset: 1, color: hexToRgb(colors[i], '0.5') }
            ]
          }
        }
      }
      return Object.assign(v, defaultSeries)
    })
  }
  return option
}

/**
 * 柱状图、条形图、堆叠图
 * @params {*}
 * @returns
 */
export const barChart = (params) => {
  const barGradientColor = [
    params.linearColor === 'Purple' ? [
      { offset: 0, color: '#5773fd' },
      { offset: 0.5, color: '#7a91fe' },
      { offset: 1, color: '#becaff' }
    ] : [
      { offset: 0, color: '#0080ff' },
      { offset: 0.5, color: '#01b9ff' },
      { offset: 1, color: '#02ddff' }
    ],
    [
      { offset: 0, color: '#feb705' },
      { offset: 0.5, color: '#fec905' },
      { offset: 1, color: '#fee605' }
    ],
    [
      { offset: 0, color: '#1dcdcc' },
      { offset: 0.5, color: '#1defcc' },
      { offset: 1, color: '#1dfecc' }
    ]
  ]
  const colors = ['#3FB1E3', '#6BE6C1', '#ff5e44', '#02fff0', '#448fff', '#7256ff', '#9967bd', '#ff9d4e']
  const option = {
    legend: {
      show: params.isShowLegend || false,
      type: 'scroll',
      orient: 'horizontal', // 水平排列
      x: 'center',
      y: 'top',
      itemWidth: 12,
      itemHeight: 10,
      pageTextStyle: {
        color: '#999999'
      },
      textStyle: {
        color: '#999999'
      }
    },
    tooltip: {
      confine: true,
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      }
    },
    grid: {
      containLabel: true,
      top: params.gridTop || 30,
      right: params.gridRight || 0,
      left: params.gridLeft || 5,
      bottom: params.gridBottom || 0
    },
    xAxis: {
      type: params.isReverse ? 'value' : 'category',
      axisLine: {
        show: true,
        lineStyle: {
          color: '#CCCCCC'
        }
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        color: '#999999',
        lineHeight: 15,
        formatter: (text) => {
          let newParamsName = ''
          const paramsNameNumber = text.length
          const provideNumber = params.wordWrapNum || 5
          const rowNumber = Math.ceil(paramsNameNumber / provideNumber)
          for (let row = 0; row < rowNumber; row++) {
            const isH = row !== rowNumber - 1 ? '\n' : ''
            newParamsName += text.substring(row * provideNumber, (row + 1) * provideNumber) + isH
          }
          return params.isXWordWrap ? newParamsName : text
        }
      },
      splitLine: {
        show: false
      },
      boundaryGap: true,
      data: params.isReverse ? [] : params.xAxisData
    },
    yAxis: {
      name: params.yName || '',
      nameTextStyle: {
        color: '#999999'
      },
      type: params.isReverse ? 'category' : 'value',
      splitLine: {
        show: true,
        lineStyle: {
          type: 'solid',
          color: '#EEEEEE'
        }
      },
      axisLine: {
        show: params.yAxisLineShow,
        lineStyle: {
          color: params.yAxisLineShow ? '#CCCCCC' : ''
        }
      },
      axisLabel: {
        show: true,
        textStyle: {
          color: '#999999'
        }
      },
      axisTick: {
        show: false
      },
      data: params.isReverse ? params.yAxisData : ''
    },
    dataZoom: [
      {
        show: params.isShowDataZoom || false,
        type: 'slider',
        start: 0,
        end: params.zoomEnd || 100
      }
    ],
    series: params.seriesData.map((v, i) => {
      const defaultSeries = {
        name: v.name || '',
        type: 'bar',
        barWidth: v.barWidth || '10',
        itemStyle: {
          color: params.isGradient ? {
            type: 'linear', x: 0, y: 1, x2: 0, y2: 0, colorStops: barGradientColor[i]
          } : colors[i]
        },
        label: {
          show: params.labelShow || false,
          color: '#fff',
          position: 'top',
          formatter: params.yAxisMax === 100 ? '{c}%' : '{c}'
        }
      }
      return Object.assign(v, defaultSeries)
    })
  }
  return option
}
